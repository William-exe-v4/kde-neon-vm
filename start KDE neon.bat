@echo off
cd ./bin
echo You came here for one purpose and that is to boot your vm
echo How much RAM do you need? Minimum 2000
set /p ram="Enter size in MB: "
if /I %ram% geq 2000 (
        echo fair enough
        ) else (
            :invalid
                echo You really think KDE neon could run on that little memory?
                echo this time, enter something 2000 or greater
                set /p ram="Enter size in MB: "
                if /I %ram% geq 10 (echo fair enough) else (goto invalid)
        )
echo How many cores do you need? If not sure, enter 2
set /p cores="Enter number of cores: "
echo Booting your vm
qemu-system-x86_64.exe -boot c -soundhw all -hda neon.img -m %ram% -smp %cores%