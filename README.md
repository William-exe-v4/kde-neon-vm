# KDE Neon vm
## What is this?
This is a pre-packaged qemu vm that virtualizes KDE Neon with batch scripts that make everything easier
## I want it
Download the [KDE Neon vm package.exe](https://gitlab.com/William.exe_v4.0/kde-neon-vm/-/blob/master/releases/KDE%20Neon%20vm%20package.exe), run it, then decide where you want it to live\
Afterwards, run `setup.bat` in the install location and then start installing
## Installing
It would take quite some time to boot, but when it finally booted, run the `Install System` shortcut on the desktop\
Make sure to install to `qemu harddisk` or `/dev/sda`. Don't worry, this is not your physical disk. Make sure to select `erase disk`
When done installing to the virtual disk, stop the vm, delete `setup.bat`, and run `start KDE Neon.bat`

## Tips
- If everything is slow, don't spam. This is normal in a vm
- When the vm boots for the first time, it boots with all sound cards enabled, 4GB RAM, and 2 cpu cores
- In case you get logged out, the default user password for the live session user is `demo`
- When booting normally after setup, it boots with all sound cards enabled. Cpu cores and RAM are user-defined