@echo off
color b
echo Hey there, it looks like you are trying to set up a KDE Neon vm. I will guide you through the process
echo so, we have to create the virtual disk first, how big do you want it to be? Minimum 10 GB
set /p size="Enter size in GB: "
if /I %size% geq 10 (
        echo fair enough
        ) else (
            :invalid
                echo You really think KDE neon can fit into something that small?
                echo this time, enter something 10 or greater
                set /p size="Enter size in GB: "
                if /I %size% geq 10 (echo fair enough) else (goto invalid)
        )
echo creating disk...
cd ./bin
start silentrun.vbs
qemu-img.exe create -f raw neon.img %size%G
if exist neon.img (
    echo disk created successfully
) else (
    echo something didn't work. You might want to check the files 
)
echo starting your vm for the first time...
qemu-system-x86_64.exe -boot d -soundhw all -cdrom neon.iso -hda neon.img -m 4096 -smp 2
sleep 120
taskkill /IM "wscript.exe" /F